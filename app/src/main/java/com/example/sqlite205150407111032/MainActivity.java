package com.example.sqlite205150407111032;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "RV1";
    Button bt1;
    Button bt2;
    EditText et1;
    EditText et2;
    TextView tv1;
    private AppDatabase appDb;

    RecyclerView rv;
    ItemAdapter adapt;
    ArrayList<Item> it;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appDb = AppDatabase.getInstance(getApplicationContext());
        bt1 = findViewById(R.id.bt1);
        bt2 = findViewById(R.id.bt2);
        et1 = findViewById(R.id.et1);
        et2 = findViewById(R.id.et2);

        rv = findViewById(R.id.rv1);
        rv.setLayoutManager(new LinearLayoutManager(this));

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Item item = new Item();
                item.setJudul(et1.getText().toString());
                item.setNim(et2.getText().toString());
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        appDb.itemDao().insertAll(item);
                    }
                });
            }
        });
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        AsyncExample task2=new AsyncExample(rv);
                        task2.execute();}
                });
            }
        });
    }
    private class AsyncExample extends AsyncTask <Void, Void, List<Item>> {

        RecyclerView rv;
        public AsyncExample(RecyclerView rv){
            this.rv = rv;
        }

        @Override
        protected List<Item> doInBackground(Void... voids) {
                List<Item> list = appDb.itemDao().getAll();
                return list;
        }

        @Override
        protected void onPostExecute(List<Item> items) {
            super.onPostExecute(items);
            adapt = new ItemAdapter(MainActivity.this, items);
            rv.setAdapter(adapt);
            adapt.notifyDataSetChanged();

        }
    }
}